/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.repositories.mongo;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.UpdateOptions;
import dev.chromie.ChromieTask;
import dev.chromie.repositories.ChromieRepository;
import dev.chromie.serialization.json.JacksonSerializer;
import lombok.NonNull;
import org.bson.Document;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import static com.mongodb.client.model.Filters.*;
import static com.mongodb.client.model.Updates.set;
import static dev.chromie.ChromieTask.State.PENDING;
import static dev.chromie.ChromieTask.State.WAITING;

public class MongoRepository implements ChromieRepository {
    @NonNull
    private MongoCollection<Document> taskCollection;

    @NonNull
    private JacksonSerializer serializer;

    MongoRepository(MongoDatabase db) {
        this(db, "chromie-tasks", new JacksonSerializer());
    }

    public MongoRepository(MongoDatabase db, String collectionName, JacksonSerializer serializer) {
        this.taskCollection = db.getCollection(collectionName); // TODO check indexes

        this.serializer = serializer;
    }

    public void save(@NonNull ChromieTask task) {
        taskCollection.replaceOne(eq("id", task.getId()),
            new Document("id", task.getId())
                .append("time", toDate(task.getTime()))
                .append("repeatAfter", task.getRepeatAfter().toString())
                .append("repeatMode", task.getRepeatMode().name())
                .append("state", task.getState().name())
                .append("task", BasicDBObject.parse(new String(serializer.serialize(task.getTask())))),
            new UpdateOptions().upsert(true)
        );
    }

    public List<ChromieTask> findByTime(@NonNull LocalDateTime time) {
        return StreamSupport.stream(taskCollection.find(lte("time", toDate(time))).map(scheduledTask -> {
            byte[] payloadBytes = scheduledTask.get("task", Document.class).toJson().getBytes();
            LocalDateTime scheduledAt = scheduledTask.getDate("time").toInstant().atZone(ZoneOffset.UTC).toLocalDateTime();
            Duration repeatAfter = Duration.parse(scheduledTask.getString("repeatAfter"));
            ChromieTask.RepeatMode repeatMode = ChromieTask.RepeatMode.valueOf(scheduledTask.getString("repeatMode"));
            ChromieTask.State state = ChromieTask.State.valueOf(scheduledTask.getString("state"));

            return new ChromieTask(scheduledTask.getString("id"), scheduledAt, serializer.deserialize(payloadBytes), repeatAfter, repeatMode, state);
        }).spliterator(), false).collect(Collectors.toList());
    }

    public boolean claim(@NonNull String id) {
        return taskCollection.updateOne(and(eq("id", id), eq("state", WAITING.name())), set("state", PENDING.name())).getModifiedCount() > 0;
    }

    private Date toDate(LocalDateTime time) {
        return Date.from(time.toInstant(ZoneOffset.UTC));
    }

    public void delete(String id) {
        taskCollection.deleteOne(eq("id", id));
    }
}
