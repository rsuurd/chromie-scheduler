/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.repositories.mongo;

import dev.chromie.ChromieTask;
import com.github.fakemongo.Fongo;
import com.mongodb.client.MongoDatabase;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class MongoRepositoryTest {
    private MongoDatabase db;

    private MongoRepository repository;

    @BeforeEach
    void createRepository() {
        Fongo fongo = new Fongo("mongo server 1");

        db = fongo.getDatabase("chromie-test");

        repository = new MongoRepository(db);
    }

    @Test
    void shouldSave() {
        repository.save(new ChromieTask("taskId", LocalDateTime.now(), new Task()));

        assertEquals(1L, db.getCollection("chromie-tasks").count());
    }

    @Test
    void shouldReschedule() {
        LocalDateTime time = LocalDateTime.now();

        repository.save(new ChromieTask("taskId", time, new Task()));
        repository.save(new ChromieTask("taskId", time.plusDays(1), new Task()));

        assertEquals(1L, db.getCollection("chromie-tasks").count());
    }

    @Test
    void shouldFind() {
        LocalDateTime time = LocalDateTime.now().plusMinutes(1).truncatedTo(ChronoUnit.MILLIS);
        repository.save(new ChromieTask("taskId", time, new Task(), Duration.ofDays(1), ChromieTask.RepeatMode.FIXED_RATE));

        assertEquals(0, repository.findByTime(LocalDateTime.now()).size());

        List<ChromieTask> tasks = repository.findByTime(LocalDateTime.now().plusMinutes(1));
        assertEquals(1, tasks.size());
        tasks.forEach(task -> {
            assertEquals("taskId", task.getId());
            assertEquals(time, task.getTime());
            assertNotNull(task.getTask());
            assertEquals(Duration.ofDays(1), task.getRepeatAfter());
            Assertions.assertEquals(ChromieTask.RepeatMode.FIXED_RATE, task.getRepeatMode());
        });
    }

    @Test
    void shouldClaim() {
        repository.save(new ChromieTask("taskId", LocalDateTime.now(), new Task()));

        assertTrue(repository.claim("taskId"));
        assertFalse(repository.claim("taskId"));
    }

    @Test
    void shouldDelete() {
        repository.save(new ChromieTask("taskId", LocalDateTime.now(), new Task()));

        repository.delete("taskId");

        assertEquals(0L, db.getCollection("chromie-tasks").count());
    }

    @AfterEach
    void dropDb() {
        db.drop();
    }

    private static class Task implements Runnable {
        public void run() {
        }
    }
}
