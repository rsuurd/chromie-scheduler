/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.repositories.jpa;

import dev.chromie.ChromieTask;
import dev.chromie.serialization.JavaSerializer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.io.Serializable;
import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class JpaRepositoryTest {
    private static EntityManagerFactory entityManagerFactory;
    private static EntityManager entityManager;

    private JpaRepository repository;

    @BeforeAll
    static void createEntityManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("chromie-test");
        entityManager = entityManagerFactory.createEntityManager();
    }

    @BeforeEach
    void createRepository() {
        repository = new JpaRepository(entityManager, new JavaSerializer());
        entityManager.getTransaction().begin();
        entityManager.createQuery("delete from ScheduledTaskEntity").executeUpdate();
        entityManager.getTransaction().commit();
        entityManager.clear();
    }

    @Test
    void shouldSave() {
        repository.save(new ChromieTask("taskId", LocalDateTime.now(), new Task()));

        assertNotNull(entityManager.find(ScheduledTaskEntity.class, "taskId"));
    }

    @Test
    void shouldClaim() {
        repository.save(new ChromieTask("taskId", LocalDateTime.now(), new Task()));

        assertTrue(repository.claim("taskId"));
        assertFalse(repository.claim("taskId"));

        assertNotNull(entityManager.find(ScheduledTaskEntity.class, "taskId"));
    }

    @Test
    void shouldReschedule() {
        LocalDateTime time = LocalDateTime.now();

        repository.save(new ChromieTask("taskId", time, new Task()));
        repository.save(new ChromieTask("taskId", time.plusDays(1), new Task()));

        ScheduledTaskEntity task = entityManager.find(ScheduledTaskEntity.class, "taskId");
        assertNotNull(task);
        assertEquals(time.plusDays(1), task.getTime());
    }

    @Test
    void shouldFind() {
        repository.save(new ChromieTask("taskId", LocalDateTime.now().plusMinutes(1), new Task()));

        assertEquals(0, repository.findByTime(LocalDateTime.now()).size());
        assertEquals(1, repository.findByTime(LocalDateTime.now().plusMinutes(1)).size());
    }

    @Test
    void shouldDelete() {
        repository.save(new ChromieTask("taskId", LocalDateTime.now(), new Task()));

        repository.delete("taskId");

        assertNull(entityManager.find(ScheduledTaskEntity.class, "taskId"));
    }

    @AfterAll
    static void close() {
        entityManager.close();
        entityManagerFactory.close();
    }

    static class Task implements Runnable, Serializable {
        public void run() {}
    }
}
