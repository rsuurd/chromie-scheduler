/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.repositories.jpa;

import dev.chromie.ChromieTask;
import dev.chromie.repositories.ChromieRepository;
import dev.chromie.serialization.Serializer;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import javax.persistence.EntityManager;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class JpaRepository implements ChromieRepository {
    @NonNull
    private EntityManager entityManager;

    @NonNull
    private Serializer serializer;

    public void save(@NonNull ChromieTask task) {
        try {
            entityManager.getTransaction().begin();
            entityManager.merge(new ScheduledTaskEntity(task.getId(), task.getTime(), serializer.serialize(task.getTask()),
                task.getRepeatAfter(), task.getRepeatMode(), task.getState()));
            entityManager.getTransaction().commit();
        } catch (Exception exception) {
            entityManager.getTransaction().rollback();
        }
    }

    public List<ChromieTask> findByTime(@NonNull LocalDateTime time) {
        return entityManager.createNamedQuery("ScheduledTaskEntity.findByTime", ScheduledTaskEntity.class)
            .setParameter("time", time)
            .getResultStream().map(scheduledTask ->
                new ChromieTask(scheduledTask.getId(), scheduledTask.getTime(), serializer.deserialize(scheduledTask.getSerializedTask()),
                    scheduledTask.getRepeatAfter(), scheduledTask.getRepeatMode(), scheduledTask.getState())
            ).collect(Collectors.toList());
    }

    public boolean claim(@NonNull String id) {
        boolean claimed;

        try {
            entityManager.getTransaction().begin();

            claimed = entityManager.createNamedQuery("ScheduledTaskEntity.claim")
                .setParameter("id", id)
                .executeUpdate() > 0;

            entityManager.getTransaction().commit();
        } catch (Exception exception) {
            entityManager.getTransaction().rollback();

            claimed = false;
        }

        return claimed;
    }

    public void delete(@NonNull String id) {
        Optional.ofNullable(entityManager.find(ScheduledTaskEntity.class, id)).ifPresent(task -> entityManager.remove(task));
    }
}
