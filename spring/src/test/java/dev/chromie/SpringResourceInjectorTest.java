/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie;

import dev.chromie.repositories.ChromieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class SpringResourceInjectorTest {
    private SpringResourceInjector injector;

    @BeforeEach
    void createInjector() {
        ApplicationContext context = new AnnotationConfigApplicationContext(Context.class);

        injector = new SpringResourceInjector();
        injector.setApplicationContext(context);
    }

    @Test
    void shouldAutowire() {
        SomeTask task = new SomeTask();

        injector.inject(task);

        assertNotNull(task.chromieRepository);
    }

    static class SomeTask implements Runnable {
        @Autowired
        private ChromieRepository chromieRepository;

        @Override
        public void run() {}
    }

    @Configuration
    static class Context {
        @Bean
        public ResourceInjector resourceInjector() {
            return new SpringResourceInjector();
        }

        @Bean
        public ChromieRepository repository() {
            return mock(ChromieRepository.class);
        }
    }
}
