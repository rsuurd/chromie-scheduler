/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;

public class SpringResourceInjector implements ResourceInjector, ApplicationContextAware {
    private AutowireCapableBeanFactory factory;

    public void inject(Object object) {
        factory.autowireBeanProperties(object, AutowireCapableBeanFactory.AUTOWIRE_NO, false);
    }

    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        factory = applicationContext.getAutowireCapableBeanFactory();
    }
}
