![build status](https://gitlab.com/rsuurd/chromie-scheduler/badges/master/build.svg?)

# Chromie

Chromie persists `ScheduledFuture`s.

## Getting started

Please refer to the [guide](https://chromie.dev/guide)