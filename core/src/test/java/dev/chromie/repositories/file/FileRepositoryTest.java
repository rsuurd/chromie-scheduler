/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.repositories.file;

import dev.chromie.ChromieTask;
import dev.chromie.serialization.Serializer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class FileRepositoryTest {
    private Path temp;

    @Mock
    private Serializer serializer;

    private FileRepository repository;

    @BeforeEach
    void createRepository() throws IOException {
        temp = Files.createTempDirectory("chromie");

        repository = new FileRepository(temp, serializer);
    }

    @Test
    void shouldSave() throws IOException {
        when(serializer.serialize(any())).thenReturn("serializedTask".getBytes());

        repository.save(new ChromieTask(TASK_ID, LocalDateTime.now().plusDays(1), (Runnable) () -> {}));

        verify(serializer).serialize(any());

        assertEquals(1L, Files.find(temp, 1, (path, matcher) -> path.getFileName().toString().startsWith(TASK_ID)).count());
    }

    @Test
    void shouldReschedule() throws IOException {
        when(serializer.serialize(any())).thenReturn("serializedTask".getBytes());

        Runnable runnable = () -> {};
        repository.save(new ChromieTask(TASK_ID, LocalDateTime.now().plusDays(1), runnable));
        repository.save(new ChromieTask(TASK_ID, LocalDateTime.now().plusDays(2), runnable));

        assertEquals(1, Files.list(temp).count());
    }

    @Test
    void shouldFindTasks() throws IOException {
        byte[] serializedTask = "serializedTask".getBytes();
        LocalDateTime scheduledTime = LocalDateTime.now().minusSeconds(1).truncatedTo(ChronoUnit.MILLIS);

        Files.write(temp.resolve(FileRepository.getFilename(TASK_ID, scheduledTime, ChromieTask.State.WAITING)), serializedTask);

        when(serializer.deserialize(serializedTask)).thenReturn(
            new FileRepository.SerializedChromieTask((Callable<Void>) () -> null, Duration.ZERO, ChromieTask.RepeatMode.ONE_SHOT)
        );

        List<ChromieTask> tasks = repository.findByTime(LocalDateTime.now());

        assertNotNull(tasks);
        assertEquals(1, tasks.size());

        ChromieTask task = tasks.get(0);
        assertEquals(TASK_ID, task.getId());
        assertEquals(scheduledTime, task.getTime());
        assertNotNull(task.getTask());
        assertTrue(task.getMaybeCallable().isPresent());
        assertFalse(task.getMaybeRunnable().isPresent());
        assertEquals(Duration.ZERO, task.getRepeatAfter());
        assertEquals(ChromieTask.RepeatMode.ONE_SHOT, task.getRepeatMode());
    }

    @Test
    void shouldClaim() throws Exception {
        Files.write(temp.resolve(FileRepository.getFilename(TASK_ID, LocalDateTime.now(), ChromieTask.State.WAITING)), "() -> null".getBytes());

        assertTrue(repository.claim(TASK_ID));
        assertFalse(repository.claim(TASK_ID));
    }

    @Test
    void shouldDeleteTask() throws IOException {
        Files.write(temp.resolve(FileRepository.getFilename(TASK_ID, LocalDateTime.now(), ChromieTask.State.WAITING)), "() -> null".getBytes());

        repository.delete(TASK_ID);

        assertEquals(0L, Files.find(temp, 1, (path, matcher) -> path.getFileName().toString().startsWith(TASK_ID)).count());
    }

    private static final String TASK_ID = "id";
}
