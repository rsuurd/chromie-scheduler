/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.concurrent;

import dev.chromie.ChromieContext;
import dev.chromie.ChromieTask;
import dev.chromie.repositories.ChromieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class ChromieScheduledExecutorServiceTest {
    private Clock clock;

    @Mock
    private ScheduledExecutorService scheduledExecutorService;
    @Mock
    private ChromieRepository repository;

    private ChromieScheduledExecutorService service;

    @BeforeEach
    void createService() {
        clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());

        service = new ChromieScheduledExecutorService(new ChromieContext(clock, scheduledExecutorService, repository));
    }

    @Test
    void shouldPersistScheduledCommand() {
        Runnable command = () -> {};

        ScheduledFuture<?> future = service.schedule(command, 1, TimeUnit.DAYS);

        assertNotNull(future);
        assertTrue(future instanceof PersistedScheduledFuture);
        assertEquals(1, future.getDelay(TimeUnit.DAYS));

        ArgumentCaptor<ChromieTask> captor = ArgumentCaptor.forClass(ChromieTask.class);

        verify(repository).save(captor.capture());

        ChromieTask task = captor.getValue();
        assertNotNull(task);
        assertNotNull(task.getId());
        assertEquals(LocalDateTime.now(clock).plusDays(1), task.getTime());
        assertTrue(task.getMaybeRunnable().isPresent());
        assertFalse(task.getMaybeCallable().isPresent());
    }

    @Test
    void shouldPersistScheduledCallable() {
        Callable<Void> callable = () -> null;

        ScheduledFuture<Void> future = service.schedule(callable, 1, TimeUnit.DAYS);

        assertNotNull(future);
        assertTrue(future instanceof PersistedScheduledFuture);
        assertEquals(1, future.getDelay(TimeUnit.DAYS));

        ArgumentCaptor<ChromieTask> captor = ArgumentCaptor.forClass(ChromieTask.class);

        verify(repository).save(captor.capture());

        ChromieTask task = captor.getValue();
        assertNotNull(task);
        assertNotNull(task.getId());
        assertEquals(LocalDateTime.now(clock).plusDays(1), task.getTime());
        assertFalse(task.getMaybeRunnable().isPresent());
        assertTrue(task.getMaybeCallable().isPresent());
    }

    @Test
    void shoulPersistCommandAtFixedRate() {
        ScheduledFuture<?> future = service.scheduleAtFixedRate(() -> {}, 1, 1, TimeUnit.DAYS);

        assertNotNull(future);
        assertTrue(future instanceof PersistedScheduledFuture);
        assertEquals(1, future.getDelay(TimeUnit.DAYS));

        ArgumentCaptor<ChromieTask> captor = ArgumentCaptor.forClass(ChromieTask.class);

        verify(repository).save(captor.capture());

        ChromieTask task = captor.getValue();
        assertNotNull(task);
        assertNotNull(task.getId());
        assertEquals(LocalDateTime.now(clock).plusDays(1), task.getTime());
        assertTrue(task.getMaybeRunnable().isPresent());
        assertFalse(task.getMaybeCallable().isPresent());
        assertEquals(ChromieTask.RepeatMode.FIXED_RATE, task.getRepeatMode());
        assertEquals(Duration.ofDays(1), task.getRepeatAfter());
    }

    @Test
    void shouldNotPersistCommandWithFixedDelay() {
        ScheduledFuture<?> future = service.scheduleWithFixedDelay(() -> {}, 1, 1, TimeUnit.HOURS);

        assertNotNull(future);
        assertTrue(future instanceof PersistedScheduledFuture);
        assertEquals(1, future.getDelay(TimeUnit.HOURS));

        ArgumentCaptor<ChromieTask> captor = ArgumentCaptor.forClass(ChromieTask.class);

        verify(repository).save(captor.capture());

        ChromieTask task = captor.getValue();
        assertNotNull(task);
        assertNotNull(task.getId());
        assertEquals(LocalDateTime.now(clock).plusHours(1), task.getTime());
        assertTrue(task.getMaybeRunnable().isPresent());
        assertFalse(task.getMaybeCallable().isPresent());
        assertEquals(ChromieTask.RepeatMode.AFTER_DELAY, task.getRepeatMode());
        assertEquals(Duration.ofHours(1), task.getRepeatAfter());
    }
}
