/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.concurrent;

import dev.chromie.ChromieContext;
import dev.chromie.ChromieException;
import dev.chromie.repositories.ChromieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.Clock;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.concurrent.Delayed;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PersistedScheduledFutureTest {
    private Clock clock;

    @Mock
    private ScheduledExecutorService scheduledExecutorService;

    @Mock
    private ChromieRepository repository;

    private PersistedScheduledFuture<Void> future;

    @BeforeEach
    void createFuture() {
        clock = Clock.fixed(Instant.now(), ZoneId.systemDefault());

        future = new PersistedScheduledFuture<>("id", LocalDateTime.now(clock).plusDays(2), new ChromieContext(clock, scheduledExecutorService, repository));
    }

    @Test
    void shouldGetDelay() {
        assertEquals(2, future.getDelay(TimeUnit.DAYS));
    }

    @Test
    void shouldCompareToOtherDelayed() {
        assertEquals(0, future.compareTo(future));

        Delayed delayed = mock(Delayed.class);
        when(delayed.getDelay(any())).thenReturn(Duration.ofDays(2).toMillis());
        assertEquals(0, future.compareTo(delayed));
        when(delayed.getDelay(any())).thenReturn(Duration.ofDays(3).toMillis());
        assertEquals(-1, future.compareTo(delayed));
        when(delayed.getDelay(any())).thenReturn(Duration.ofDays(1).toMillis());
        assertEquals(1, future.compareTo(delayed));
    }

    @Test
    void shouldCancelUnclaimedTask() {
        when(repository.claim("id")).thenReturn(true);

        assertTrue(future.cancel(true));

        verify(repository).delete("id");
    }

    @Test
    void shouldNotCancelCancelledTask() {
        PersistedScheduledFuture<?> cancelledFuture = Mockito.spy(future);
        when(cancelledFuture.isCancelled()).thenReturn(true);

        assertFalse(cancelledFuture.cancel(true));

        verifyZeroInteractions(repository);
    }

    @Test
    void shouldNotCancelClaimedTask() {
        when(repository.claim("id")).thenReturn(false);

        assertFalse(future.cancel(true));

        verifyZeroInteractions(repository);
    }

    @Test
    void shouldThrowChromieExceptionForIsDone() {
        assertThrows(ChromieException.class, () -> future.isDone());
    }

    @Test
    void shouldThrowChromieExceptionForGet() {
        assertThrows(ChromieException.class, () -> future.get());
    }

    @Test
    void shouldThrowChromieExceptionForGetWithTimeout() {
        assertThrows(ChromieException.class, () -> future.get(1, TimeUnit.MILLISECONDS));
    }
}
