/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.concurrent;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

import static java.util.Collections.emptyList;
import static java.util.Collections.singleton;
import static java.util.Collections.singletonList;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyCollection;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DelegatingExecutorServiceTest {
    @Mock
    private ExecutorService delegate;

    @Mock
    private Future<?> future;

    @InjectMocks
    private DelegatingExecutorService executorService;

    @Test
    void shouldDelegateShutdown() {
        executorService.shutdown();

        verify(delegate).shutdown();
    }

    @Test
    void shouldDelegateShutdownNow() {
        when(delegate.shutdownNow()).thenReturn(emptyList());

        assertSame(emptyList(), executorService.shutdownNow());

        verify(delegate).shutdownNow();
    }

    @Test
    void shouldDelegateIsShutdown() {
        when(delegate.isShutdown()).thenReturn(true);

        assertTrue(executorService.isShutdown());

        verify(delegate).isShutdown();
    }

    @Test
    void shouldDelegateIsTerminated() {
        when(delegate.isTerminated()).thenReturn(true);

        assertTrue(executorService.isTerminated());

        verify(delegate).isTerminated();
    }

    @Test
    void shouldDelegateAwaitTermination() throws InterruptedException {
        when(delegate.awaitTermination(anyLong(), any())).thenReturn(true);

        assertTrue(executorService.awaitTermination(1L, TimeUnit.MINUTES));

        verify(delegate).awaitTermination(1L, TimeUnit.MINUTES);
    }

    @Test
    @SuppressWarnings("unchecked")
    void shouldDelegateSubmitCallable() {
        doReturn(future).when(delegate).submit(any(Callable.class));

        assertSame(future, executorService.submit(() -> null));

        verify(delegate).submit(any(Callable.class));
    }

    @Test
    void shouldDelegateSubmitRunnableWithResult() {
        doReturn(future).when(delegate).submit(any(Runnable.class), any());

        assertSame(future, executorService.submit(() -> {
        }, null));

        verify(delegate).submit(any(Runnable.class), any());
    }

    @Test
    void shouldDelegateSubmitRunnable() {
        doReturn(future).when(delegate).submit(any(Runnable.class));

        assertSame(future, executorService.submit(() -> {
        }));

        verify(delegate).submit(any(Runnable.class));
    }

    @Test
    void shouldDelegateInvokeAll() throws InterruptedException {
        doReturn(singletonList(future)).when(delegate).invokeAll(anyCollection());

        assertEquals(singletonList(future), executorService.invokeAll(singleton(() -> null)));

        verify(delegate).invokeAll(anyCollection());
    }

    @Test
    void shouldDelegateInvokeAllWithTimeout() throws InterruptedException {
        doReturn(singletonList(future)).when(delegate).invokeAll(anyCollection(), anyLong(), any());

        assertEquals(singletonList(future), executorService.invokeAll(singleton(() -> null), 1L, TimeUnit.MINUTES));

        verify(delegate).invokeAll(anyCollection(), eq(1L), eq(TimeUnit.MINUTES));
    }

    @Test
    void shouldDelegateInvokeAny() throws InterruptedException, ExecutionException {
        doReturn(singletonList(future)).when(delegate).invokeAny(anyCollection());

        assertEquals(singletonList(future), executorService.invokeAny(singleton(() -> null)));

        verify(delegate).invokeAny(anyCollection());
    }

    @Test
    void shouldDelegateInvokeAnyWithTimeout() throws InterruptedException, ExecutionException, TimeoutException {
        doReturn(singletonList(future)).when(delegate).invokeAny(anyCollection(), anyLong(), any());

        assertEquals(singletonList(future), executorService.invokeAny(singleton(() -> null), 1L, TimeUnit.MINUTES));

        verify(delegate).invokeAny(anyCollection(), eq(1L), eq(TimeUnit.MINUTES));
    }
}
