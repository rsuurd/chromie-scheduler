/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.Callable;

import static java.time.Duration.ZERO;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ChromieTaskTest {
    @Test
    void shouldAcceptRunnable() {
        ChromieTask task = new ChromieTask("id", LocalDateTime.now(), (Runnable) () -> {});

        assertNotNull(task.getTask());
        assertTrue(task.getMaybeRunnable().isPresent());
        assertFalse(task.getMaybeCallable().isPresent());
        assertEquals(ZERO, task.getRepeatAfter());
        Assertions.assertEquals(ChromieTask.RepeatMode.ONE_SHOT, task.getRepeatMode());
        assertFalse(task.isRepeatedAtFixedRate());
        assertFalse(task.isRepeatedAfterDelay());
        Assertions.assertEquals(ChromieTask.State.WAITING, task.getState());
    }

    @Test
    void shouldAcceptCallable() {
        ChromieTask task = new ChromieTask("id", LocalDateTime.now(), (Callable<?>) () -> null);

        assertNotNull(task.getTask());
        assertFalse(task.getMaybeRunnable().isPresent());
        assertTrue(task.getMaybeCallable().isPresent());
        assertEquals(ZERO, task.getRepeatAfter());
        Assertions.assertEquals(ChromieTask.RepeatMode.ONE_SHOT, task.getRepeatMode());
        assertFalse(task.isRepeatedAtFixedRate());
        assertFalse(task.isRepeatedAfterDelay());
        Assertions.assertEquals(ChromieTask.State.WAITING, task.getState());
    }

    @Test
    void shouldNotAcceptNonRunnableOrCallable() {
        assertThrows(ChromieException.class, () -> new ChromieTask("id", LocalDateTime.now(), new Object()));
    }

    @Test
    void shouldNotAcceptNegativeRepeatAfter() {
        assertThrows(IllegalArgumentException.class, () ->
            new ChromieTask("id", LocalDateTime.now(), (Runnable) () -> {}, Duration.ofHours(-1), ChromieTask.RepeatMode.ONE_SHOT));
    }

    @Test
    void shouldAcceptRepeatedAtFixedRate() {
        ChromieTask task = new ChromieTask("id", LocalDateTime.now(), (Runnable) () -> {}, Duration.ofDays(1), ChromieTask.RepeatMode.FIXED_RATE);

        assertNotNull(task.getTask());
        assertEquals(Duration.ofDays(1), task.getRepeatAfter());
        assertTrue(task.isRepeatedAtFixedRate());
        assertFalse(task.isRepeatedAfterDelay());
    }

    @Test
    void shouldAcceptRepeatedAfterDelay() {
        ChromieTask task = new ChromieTask("id", LocalDateTime.now(), (Runnable) () -> {}, Duration.ofDays(1), ChromieTask.RepeatMode.AFTER_DELAY);

        assertNotNull(task.getTask());
        assertEquals(Duration.ofDays(1), task.getRepeatAfter());
        assertFalse(task.isRepeatedAtFixedRate());
        assertTrue(task.isRepeatedAfterDelay());
    }

    @Test
    void shouldNotAccepRepeatedTaskWithoutRepeatDuration() {
        assertThrows(IllegalArgumentException.class, () ->
            new ChromieTask("id", LocalDateTime.now(), (Runnable) () -> {}, ZERO, ChromieTask.RepeatMode.FIXED_RATE));
        assertThrows(IllegalArgumentException.class, () ->
            new ChromieTask("id", LocalDateTime.now(), (Runnable) () -> {}, ZERO, ChromieTask.RepeatMode.AFTER_DELAY));
    }

    @Test
    void shouldReschedule() {
        LocalDateTime tomorrow = LocalDateTime.now().plusDays(1);

        ChromieTask task = new ChromieTask("id", LocalDateTime.now(), (Runnable) () -> {}, Duration.ofDays(1), ChromieTask.RepeatMode.AFTER_DELAY)
            .reschedule(tomorrow);

        assertEquals(tomorrow, task.getTime());
        Assertions.assertEquals(ChromieTask.State.WAITING, task.getState());
    }
}
