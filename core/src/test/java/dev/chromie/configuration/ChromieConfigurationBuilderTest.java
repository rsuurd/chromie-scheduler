/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.configuration;

import dev.chromie.ResourceInjector;
import dev.chromie.repositories.ChromieRepository;
import dev.chromie.repositories.file.FileRepository;
import org.junit.jupiter.api.Test;

import java.time.Clock;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;

class ChromieConfigurationBuilderTest {
    @Test
    void shouldApplyDefaults() {
        ChromieConfiguration configuration = new ChromieConfigurationBuilder().build();

        assertNotNull(configuration.getClock());
        assertNotNull(configuration.getScheduledExecutorService());
        assertTrue(configuration.getRepository() instanceof FileRepository);
        assertEquals(Duration.ofMillis(200), configuration.getSweepInterval());
        assertTrue(configuration.getResourceInjectors().isEmpty());
    }

    @Test
    void shouldConfigureClock() {
        Clock clock = Clock.systemUTC();

        assertSame(clock, new ChromieConfigurationBuilder().withClock(clock).build().getClock());
    }

    @Test
    void shouldConfigureScheduledExecutorService() {
        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();

        assertSame(executorService, new ChromieConfigurationBuilder().withScheduledExecutorService(executorService).build().getScheduledExecutorService());
    }

    @Test
    void shouldConfigureRepository() {
        ChromieRepository repository = mock(ChromieRepository.class);

        assertSame(repository, new ChromieConfigurationBuilder().withRepository(repository).build().getRepository());
    }

    @Test
    void shouldConfigureSweepInterval() {
        Duration sweepInterval = Duration.ofSeconds(1);

        assertSame(sweepInterval, new ChromieConfigurationBuilder().withSweepInterval(sweepInterval).build().getSweepInterval());
    }

    @Test
    void shouldConfigureResourceInjectors() {
        ResourceInjector injector = mock(ResourceInjector.class);
        List<ResourceInjector> injectors = new ChromieConfigurationBuilder().withResourceInjector(injector).build().getResourceInjectors();

        assertEquals(1, injectors.size());
        assertSame(injector, injectors.get(0));
    }
}
