/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie;

import dev.chromie.repositories.ChromieRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.*;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import static java.time.temporal.ChronoUnit.MILLIS;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ChromieDaemonTest {
    @Mock
    private ScheduledExecutorService scheduledExecutorService;

    private Duration interval;

    private TinkerableClock clock;

    @Mock
    private ChromieRepository repository;

    @Mock
    private ResourceInjector resourceInjector;
    @Mock
    private TaskListener taskListener;

    private ChromieDaemon daemon;

    @BeforeEach
    void createDaemon() {
        interval = Duration.ofSeconds(1);

        clock = new TinkerableClock(Instant.now());

        ChromieContext context = new ChromieContext(clock, scheduledExecutorService, repository);
        daemon = new ChromieDaemon(context, interval, Collections.singletonList(resourceInjector), Collections.singletonList(taskListener));
    }

    @Test
    void shouldStartDaemon() {
        daemon.start();

        verify(scheduledExecutorService).scheduleAtFixedRate(any(Runnable.class), eq(0L), eq(1000L), eq(TimeUnit.MILLISECONDS));
    }

    @Test
    void shouldScheduleTasks() {
        Runnable runnable = () -> {};
        Callable<Void> callable = () -> null;

        List<ChromieTask> tasks = Arrays.asList(
            new ChromieTask("first", LocalDateTime.now(clock).plus(100, MILLIS), runnable),
            new ChromieTask("second", LocalDateTime.now(clock).plus(200, MILLIS), callable)
        );

        when(repository.findByTime(LocalDateTime.now(clock).plus(interval))).thenReturn(tasks);
        when(repository.claim(any())).thenReturn(true);

        daemon.sweep();

        verify(scheduledExecutorService).schedule(isA(Runnable.class), eq(100L), eq(TimeUnit.MILLISECONDS));
        verify(scheduledExecutorService).schedule(isA(Runnable.class), eq(200L), eq(TimeUnit.MILLISECONDS));

        verifyZeroInteractions(resourceInjector, taskListener);
    }

    @Test
    void shouldNotScheduleUnclaimedTasks() {
        ChromieTask task = new ChromieTask("task", LocalDateTime.now(clock), (Runnable) () -> {});

        when(repository.findByTime(LocalDateTime.now(clock).plus(interval))).thenReturn(Collections.singletonList(task));
        when(repository.claim("task")).thenReturn(false);

        daemon.sweep();

        verifyZeroInteractions(scheduledExecutorService, resourceInjector, taskListener);
    }

    @Test
    void shouldRescheduleAtFixedRate() {
        LocalDateTime time = LocalDateTime.now(clock).plus(100, MILLIS);

        when(repository.findByTime(LocalDateTime.now(clock).plus(interval))).thenReturn(Collections.singletonList(
            new ChromieTask("daily", time, (Runnable) () -> {}, Duration.ofDays(1), ChromieTask.RepeatMode.FIXED_RATE)
        ));
        when(repository.claim(any())).thenReturn(true);

        daemon.sweep();

        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);

        verify(scheduledExecutorService).schedule(runnableCaptor.capture(), eq(100L), eq(TimeUnit.MILLISECONDS));

        runnableCaptor.getValue().run();

        verify(resourceInjector).inject(any());
        verify(taskListener).onComplete(any(), any());
        verify(taskListener, never()).onException(any(), any());

        ArgumentCaptor<ChromieTask> taskCaptor = ArgumentCaptor.forClass(ChromieTask.class);

        verify(repository).save(taskCaptor.capture());

        ChromieTask task = taskCaptor.getValue();
        assertNotNull(task);
        assertEquals("daily", task.getId());
        assertEquals(ChromieTask.RepeatMode.FIXED_RATE, task.getRepeatMode());
        assertEquals(time.plusDays(1), task.getTime());
    }

    @Test
    void shouldRescheduleAfterDelay() {
        LocalDateTime time = LocalDateTime.now(clock).plus(100, MILLIS);

        when(repository.findByTime(LocalDateTime.now(clock).plus(interval))).thenReturn(Collections.singletonList(
            new ChromieTask("after_an_hour", time, (Runnable) () -> {}, Duration.ofHours(1), ChromieTask.RepeatMode.AFTER_DELAY)
        ));
        when(repository.claim(any())).thenReturn(true);

        daemon.sweep();

        clock.fastForward(Duration.ofSeconds(4));

        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);

        verify(scheduledExecutorService).schedule(runnableCaptor.capture(), eq(100L), eq(TimeUnit.MILLISECONDS));

        runnableCaptor.getValue().run();

        verify(resourceInjector).inject(any());
        verify(taskListener).onComplete(any(), any());
        verify(taskListener, never()).onException(any(), any());

        ArgumentCaptor<ChromieTask> taskCaptor = ArgumentCaptor.forClass(ChromieTask.class);

        verify(repository).save(taskCaptor.capture());

        ChromieTask task = taskCaptor.getValue();
        assertNotNull(task);
        assertEquals("after_an_hour", task.getId());
        assertEquals(ChromieTask.RepeatMode.AFTER_DELAY, task.getRepeatMode());

        assertEquals(LocalDateTime.now(clock).plusHours(1), task.getTime());
    }

    @Test
    void shouldDeleteWhenDone() {
        when(repository.findByTime(LocalDateTime.now(clock).plus(interval))).thenReturn(Collections.singletonList(
            new ChromieTask("taskId", LocalDateTime.now(clock).plus(100, MILLIS), (Runnable) () -> {})
        ));
        when(repository.claim(any())).thenReturn(true);

        daemon.sweep();

        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);

        verify(scheduledExecutorService).schedule(runnableCaptor.capture(), eq(100L), eq(TimeUnit.MILLISECONDS));

        runnableCaptor.getValue().run();

        verify(resourceInjector).inject(any());
        verify(taskListener).onComplete(any(), any());
        verify(taskListener, never()).onException(any(), any());

        verify(repository).delete("taskId");
    }

    @Test
    void shouldInformListenersOfFailedExecution() {
        when(repository.findByTime(LocalDateTime.now(clock).plus(interval))).thenReturn(Collections.singletonList(
            new ChromieTask("taskId", LocalDateTime.now(clock).plus(100, MILLIS), (Runnable) () -> {
                throw new RuntimeException();
            }
        )));

        when(repository.claim(any())).thenReturn(true);

        daemon.sweep();

        ArgumentCaptor<Runnable> runnableCaptor = ArgumentCaptor.forClass(Runnable.class);

        verify(scheduledExecutorService).schedule(runnableCaptor.capture(), eq(100L), eq(TimeUnit.MILLISECONDS));

        runnableCaptor.getValue().run();

        verify(taskListener, never()).onComplete(any(), any());
        verify(taskListener).onException(any(), any());
    }

    @Test
    void shouldShutdownDaemon() {
        daemon.shutdown();

        verify(scheduledExecutorService).shutdown();
    }

    private static class TinkerableClock extends Clock {
        private Instant instant;
        private ZoneId zone;

        private TinkerableClock(Instant instant) {
            this.instant = instant;
            this.zone = ZoneId.systemDefault();
        }

        @Override
        public ZoneId getZone() {
            return zone;
        }

        @Override
        public Clock withZone(ZoneId zone) {
            return new TinkerableClock(instant);
        }

        @Override
        public Instant instant() {
            return instant;
        }

        @Override
        public long millis() {
            return instant.toEpochMilli();
        }

        void fastForward(Duration duration) {
            instant = instant.plus(duration);
        }
    }
}
