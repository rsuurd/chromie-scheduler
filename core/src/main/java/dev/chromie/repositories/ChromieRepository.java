/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.repositories;

import dev.chromie.ChromieTask;
import lombok.NonNull;

import java.time.LocalDateTime;
import java.util.List;

public interface ChromieRepository {
    // TODO split into persist / reschedule
    void save(@NonNull ChromieTask task);

    List<ChromieTask> findByTime(@NonNull LocalDateTime time);

    boolean claim(@NonNull String id);

    void delete(@NonNull String id);
}
