/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.repositories.file;

import dev.chromie.ChromieException;
import dev.chromie.ChromieTask;
import dev.chromie.repositories.ChromieRepository;
import dev.chromie.serialization.JavaSerializer;
import dev.chromie.serialization.Serializer;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.IOException;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Stream;

public class FileRepository implements ChromieRepository {
    @NonNull
    private Path path;

    @NonNull
    private Serializer serializer;

    private ReentrantLock fileLock;

    public FileRepository() {
        this(Paths.get("chromie"), new JavaSerializer());
    }

    public FileRepository(@NonNull Path path, @NonNull Serializer serializer) {
        this.path = path;
        this.serializer = serializer;

        if (Files.notExists(path)) {
            try {
                Files.createDirectories(path);
            } catch (IOException exception) {
                throw new ChromieException("Could not create missing directories", exception);
            }
        }

        fileLock = new ReentrantLock();
    }

    public void save(@NonNull ChromieTask task) {
        try {
            findById(task.getId()).ifPresent(path -> delete(task.getId()));

            Path taskPath = path.resolve(getFilename(task.getId(), task.getTime(), task.getState()));

            Files.write(taskPath, serializer.serialize(new SerializedChromieTask(task)));
        } catch (IOException exception) {
            throw new ChromieException("Could not save task", exception);
        }
    }

    public List<ChromieTask> findByTime(@NonNull LocalDateTime time) {
        try (Stream<Path> list = Files.list(path)) {
            List<ChromieTask> tasks = new LinkedList<>();

            list.forEach(path -> {
                String filename = path.getFileName().toString();

                int timeSeparator = filename.indexOf('-');
                int stateSeparator = filename.indexOf('-', timeSeparator + 1);

                String id = filename.substring(0, timeSeparator);
                LocalDateTime scheduledTime = Instant.ofEpochMilli(Long.parseLong(filename.substring(timeSeparator + 1, stateSeparator))).atZone(ZoneOffset.UTC).toLocalDateTime();
                ChromieTask.State state = ChromieTask.State.valueOf(filename.substring(stateSeparator + 1));

                if (!time.isBefore(scheduledTime)) {
                    try {
                        SerializedChromieTask serializedTask = serializer.deserialize(Files.readAllBytes(path));

                        tasks.add(new ChromieTask(id, scheduledTime, serializedTask.getTask(),
                            serializedTask.getRepeatAfter(), serializedTask.getRepeatMode(), state));
                    } catch (IOException exception) {
                        throw new ChromieException("Could not read task", exception);
                    }
                }
            });

            return Collections.unmodifiableList(tasks);
        } catch (IOException exception) {
            throw new ChromieException("Could not find tasks", exception);
        }
    }

    @Override
    public boolean claim(@NonNull String id) {
        try {
            fileLock.lock();

            return findById(id).filter(path -> path.toString().endsWith(ChromieTask.State.WAITING.name())).map(path -> {
                try {
                    Files.move(path, this.path.resolve(path.toString()
                        .replace(ChromieTask.State.WAITING.name(), ChromieTask.State.PENDING.name())));

                    return true;
                } catch (IOException e) {
                    return false;
                }
            }).orElse(false);
        } finally {
            fileLock.unlock();
        }
    }

    public void delete(@NonNull String id) {
        findById(id).ifPresent(path -> {
            try {
                Files.delete(path);
            } catch (IOException exception) {
                throw new ChromieException("Could not delete task", exception);
            }
        });
    }

    private Optional<Path> findById(String id) {
        try {
            return Files.find(path, Integer.MAX_VALUE, (path, basicFileAttributes) -> path.getFileName().toString().startsWith(id)).findAny();
        } catch (IOException exception) {
            throw new ChromieException("Could not find task", exception);
        }
    }

    static String getFilename(String id, LocalDateTime scheduledTime, ChromieTask.State state) {
        return String.join("-", id, Long.toString(scheduledTime.toInstant(ZoneOffset.UTC).toEpochMilli()), state.name());
    }

    @Getter
    @RequiredArgsConstructor(access = AccessLevel.PACKAGE)
    static class SerializedChromieTask implements Serializable {
        private @NonNull Object task;
        private @NonNull Duration repeatAfter;
        private @NonNull ChromieTask.RepeatMode repeatMode;

        private SerializedChromieTask(ChromieTask task) {
            this.task = task.getTask();

            repeatAfter = task.getRepeatAfter();
            repeatMode = task.getRepeatMode();
        }
    }
}
