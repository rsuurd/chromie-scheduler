package dev.chromie;

public interface TaskListener {
    default <V> void onComplete(Class<?> taskType, V value) {}

    default void onException(Class<?> taskType, Exception exception) {}
}
