/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie;

import lombok.Getter;
import lombok.NonNull;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.concurrent.Callable;

public class ChromieTask {
    @Getter
    @NonNull
    private String id;

    @Getter
    @NonNull
    private LocalDateTime time;

    private Runnable runnable;
    private Callable<?> callable;

    @Getter
    @NonNull
    private RepeatMode repeatMode;

    @Getter
    @NonNull
    private Duration repeatAfter;

    @Getter
    @NonNull
    private State state;

    public ChromieTask(String id, LocalDateTime time, Object task) {
        this(id, time, task, Duration.ZERO, RepeatMode.ONE_SHOT, State.WAITING);
    }

    public ChromieTask(String id, LocalDateTime time, Object task, Duration repeatAfter, RepeatMode repeatMode) {
        this(id, time, task, repeatAfter, repeatMode, State.WAITING);
    }

    public ChromieTask(String id, LocalDateTime time, Object task, Duration repeatAfter, RepeatMode repeatMode, State state) {
        this.id = id;
        this.time = time;

        if (task instanceof Runnable) {
            runnable = (Runnable) task;
        } else if (task instanceof Callable<?>) {
            callable = (Callable<?>) task;
        } else {
            throw new ChromieException(String.format("Task '%s' should be Runnable or Callable", task.getClass().getName()));
        }

        if (repeatAfter.isNegative()) {
            throw new IllegalArgumentException("Negative repeat duration is not allowed");
        } else if ((repeatMode != RepeatMode.ONE_SHOT) && repeatAfter.isZero()) {
            throw new IllegalArgumentException("Repeat duration may not be 0 for repeated task");
        } else {
            this.repeatAfter = repeatAfter;
            this.repeatMode = repeatMode;
        }

        this.state = state;
    }

    public ChromieTask reschedule(LocalDateTime time) {
        this.time = time;

        return withState(State.WAITING);
    }

    public ChromieTask withState(State state) {
        this.state = state;

        return this;
    }

    public Optional<Runnable> getMaybeRunnable() {
        return Optional.ofNullable(runnable);
    }

    public Optional<Callable<?>> getMaybeCallable() {
        return Optional.ofNullable(callable);
    }

    public Object getTask() {
        return (runnable == null) ? callable : runnable;
    }

    public boolean isRepeatedAtFixedRate() {
        return repeatMode == RepeatMode.FIXED_RATE;
    }

    public boolean isRepeatedAfterDelay() {
        return repeatMode == RepeatMode.AFTER_DELAY;
    }

    public boolean isOneShot() {
        return repeatMode == RepeatMode.ONE_SHOT;
    }

    public enum RepeatMode {
        ONE_SHOT, FIXED_RATE, AFTER_DELAY
    }

    public enum State {
        WAITING, PENDING
    }
}
