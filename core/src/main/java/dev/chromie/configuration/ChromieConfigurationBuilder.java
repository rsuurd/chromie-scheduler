/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.configuration;

import dev.chromie.ResourceInjector;
import dev.chromie.TaskListener;
import dev.chromie.repositories.ChromieRepository;
import dev.chromie.repositories.file.FileRepository;
import lombok.NonNull;

import java.time.Clock;
import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.function.Supplier;
import java.util.stream.Collectors;

class ChromieConfigurationBuilder {
    private Map<Class<?>, Object> components;

    ChromieConfigurationBuilder() {
        components = new HashMap<>();
    }

    public <Builder extends ChromieConfigurationBuilder> Builder withClock(Clock clock) {
        return with(Clock.class, clock);
    }

    public <Builder extends ChromieConfigurationBuilder> Builder withScheduledExecutorService(ScheduledExecutorService scheduledExecutorService) {
        return with(ScheduledExecutorService.class, scheduledExecutorService);
    }

    public <Builder extends ChromieConfigurationBuilder> Builder withRepository(ChromieRepository repository) {
        return with(ChromieRepository.class, repository);
    }

    public <Builder extends ChromieConfigurationBuilder> Builder withSweepInterval(Duration sweepInterval) {
        return with(Duration.class, sweepInterval);
    }

    public <Builder extends ChromieConfigurationBuilder> Builder withResourceInjector(ResourceInjector resourceInjector) {
        return with(resourceInjector.getClass(), resourceInjector);
    }

    @SuppressWarnings("unchecked")
    protected <Builder extends ChromieConfigurationBuilder> Builder with(Class<?> type, @NonNull Object component) {
        components.put(type, component);

        return (Builder) this;
    }

    public ChromieConfiguration build() {
        Clock clock = resolveComponent(Clock.class, Clock::systemDefaultZone);
        ScheduledExecutorService scheduledExecutorService = resolveComponent(ScheduledExecutorService.class, () ->
            Executors.newScheduledThreadPool(DEFAULT_THREAD_COUNT)
        );
        ChromieRepository repository = resolveComponent(ChromieRepository.class, FileRepository::new);
        Duration sweepInterval = resolveComponent(Duration.class, () -> DEFAULT_SWEEP_INTERVAL);
        List<ResourceInjector> resourceInjectors = components.values().stream().filter(ResourceInjector.class::isInstance).map(ResourceInjector.class::cast).collect(Collectors.toList());
        List<TaskListener> taskListeners = components.values().stream().filter(TaskListener.class::isInstance).map(TaskListener.class::cast).collect(Collectors.toList());

        return new ChromieConfiguration(clock, scheduledExecutorService, repository, sweepInterval, resourceInjectors, taskListeners);
    }

    @SuppressWarnings("unchecked")
    protected <T> T resolveComponent(Class<T> type, Supplier<T> defaultValueSupplier) {
        return (components.containsKey(type)) ? (T) components.get(type) : defaultValueSupplier.get();
    }

    private static final int DEFAULT_THREAD_COUNT = 10;
    private static final Duration DEFAULT_SWEEP_INTERVAL = Duration.ofMillis(200);
}
