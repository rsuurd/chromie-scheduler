/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.configuration;

import dev.chromie.ResourceInjector;
import dev.chromie.TaskListener;
import dev.chromie.repositories.ChromieRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;

import java.time.Clock;
import java.time.Duration;
import java.util.List;
import java.util.concurrent.ScheduledExecutorService;

@Getter
@AllArgsConstructor
public class ChromieConfiguration {
    @NonNull
    private Clock clock;

    @NonNull
    private ScheduledExecutorService scheduledExecutorService;

    @NonNull
    private ChromieRepository repository;

    @NonNull
    private Duration sweepInterval;

    @NonNull
    private List<ResourceInjector> resourceInjectors;

    @NonNull
    private List<TaskListener> taskListeners;

    public static ChromieConfiguration defaultConfiguration() {
        return builder().build();
    }

    public static ChromieConfigurationBuilder builder() {
        return new ChromieConfigurationBuilder();
    }
}
