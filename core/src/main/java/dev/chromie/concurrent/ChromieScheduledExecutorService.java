/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.concurrent;

import dev.chromie.ChromieContext;
import dev.chromie.ChromieException;
import dev.chromie.ChromieTask;
import lombok.NonNull;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;
import java.util.concurrent.Callable;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class ChromieScheduledExecutorService extends DelegatingExecutorService implements ScheduledExecutorService {
    private ChromieContext context;

    public ChromieScheduledExecutorService(@NonNull ChromieContext context) {
        super(context.getScheduledExecutorService());

        this.context = context;
    }

    public ScheduledFuture<?> schedule(Runnable command, long delay, TimeUnit unit) {
        return scheduleTask(command, delay, unit, Duration.ZERO, ChromieTask.RepeatMode.ONE_SHOT);
    }

    public <V> ScheduledFuture<V> schedule(Callable<V> callable, long delay, TimeUnit unit) {
        return scheduleTask(callable, delay, unit, Duration.ZERO, ChromieTask.RepeatMode.ONE_SHOT);
    }

    private <V> ScheduledFuture<V> scheduleTask(Object task, long delay, TimeUnit unit, Duration repeatAfter, ChromieTask.RepeatMode repeatMode) {
        String id = UUID.randomUUID().toString();
        LocalDateTime time = toLocalDateTime(delay, unit);

        context.getRepository().save(new ChromieTask(id, time, task, repeatAfter, repeatMode));

        return new PersistedScheduledFuture<>(id, time, context);
    }

    public ScheduledFuture<?> scheduleAtFixedRate(Runnable command, long initialDelay, long period, TimeUnit unit) {
        return scheduleTask(command, initialDelay, unit, Duration.of(period, toChronoUnit(unit)), ChromieTask.RepeatMode.FIXED_RATE);
    }

    public ScheduledFuture<?> scheduleWithFixedDelay(Runnable command, long initialDelay, long delay, TimeUnit unit) {
        return scheduleTask(command, initialDelay, unit, Duration.of(delay, toChronoUnit(unit)), ChromieTask.RepeatMode.AFTER_DELAY);
    }

    private LocalDateTime toLocalDateTime(long delay, TimeUnit unit) {
        return LocalDateTime.now(context.getClock()).plus(delay, toChronoUnit(unit));
    }

    private ChronoUnit toChronoUnit(TimeUnit unit) {
        switch (unit) {
            case NANOSECONDS:
                return ChronoUnit.NANOS;
            case MICROSECONDS:
                return ChronoUnit.MICROS;
            case MILLISECONDS:
                return ChronoUnit.MILLIS;
            case SECONDS:
                return ChronoUnit.SECONDS;
            case MINUTES:
                return ChronoUnit.MINUTES;
            case HOURS:
                return ChronoUnit.HOURS;
            case DAYS:
                return ChronoUnit.DAYS;
            default:
                throw new ChromieException(String.format("Could not convert %s to ChronoUnit", unit));
        }
    }
}
