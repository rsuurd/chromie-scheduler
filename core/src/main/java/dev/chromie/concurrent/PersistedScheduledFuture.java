/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.concurrent;

import dev.chromie.ChromieContext;
import dev.chromie.ChromieException;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.concurrent.Delayed;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.MILLISECONDS;

@RequiredArgsConstructor
public class PersistedScheduledFuture<V> implements ScheduledFuture<V> {
    @NonNull
    private String id;
    @NonNull
    private LocalDateTime time;

    @NonNull
    private ChromieContext context;

    private boolean cancelled;

    public long getDelay(TimeUnit unit) {
        Duration between = Duration.between(LocalDateTime.now(context.getClock()), time);

        return unit.convert(between.toMillis(), MILLISECONDS);
    }

    public int compareTo(Delayed other) {
        return Objects.compare(this, other, (o1, o2) -> {
            long diff = o1.getDelay(MILLISECONDS) - o2.getDelay(MILLISECONDS);

            return (diff == 0) ? 0 : (diff < 0) ? -1 : 1;
        });
    }

    public boolean cancel(boolean mayInterruptIfRunning) {
        if (!isCancelled() && context.getRepository().claim(id)) {
            context.getRepository().delete(id);

            cancelled = true;

            return true;
        }

        return false;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public boolean isDone() {
        throw new ChromieException("Can not determine if persisted scheduled future is done");
    }

    public V get() {
        throw new ChromieException("Can not get persisted scheduled future");
    }

    public V get(long timeout, TimeUnit unit) {
        throw new ChromieException("Can not get persisted scheduled future");
    }
}
