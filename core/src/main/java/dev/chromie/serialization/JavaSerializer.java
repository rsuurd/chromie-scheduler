/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.serialization;

import dev.chromie.ChromieException;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class JavaSerializer implements Serializer {
    public byte[] serialize(Object object) {
        try (ByteArrayOutputStream out = new ByteArrayOutputStream(); ObjectOutputStream stream = new ObjectOutputStream(out)) {
            stream.writeObject(object);

            return out.toByteArray();
        } catch (IOException exception) {
            throw new ChromieException("Could not serialize task", exception);
        }
    }

    @SuppressWarnings("unchecked")
    public <T> T deserialize(byte[] source) {
        try (ObjectInputStream input = new ObjectInputStream(new ByteArrayInputStream(source))) {
            return (T) input.readObject();
        } catch (Exception exception) {
            throw new ChromieException("could not deserialize callable", exception);
        }
    }
}
