/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie;

import dev.chromie.repositories.ChromieRepository;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

import java.time.Clock;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

@Slf4j
@RequiredArgsConstructor
public class ChromieDaemon {
    @NonNull
    private ChromieContext context;

    @NonNull
    private Duration interval;

    @NonNull
    private List<ResourceInjector> injectors;

    @NonNull
    private List<TaskListener> listeners;

    public void start() {
        context.getScheduledExecutorService().scheduleAtFixedRate(this::sweep, 0L, interval.toMillis(), TimeUnit.MILLISECONDS);
    }

    void sweep() {
        try {
            ChromieRepository repository = context.getRepository();
            Clock clock = context.getClock();

            repository.findByTime(LocalDateTime.now(clock).plus(interval)).stream().filter(task ->
                repository.claim(task.getId())
            ).forEach(task -> {
                Duration delay = Duration.between(LocalDateTime.now(clock), task.getTime());

                context.getScheduledExecutorService().schedule(() -> {
                    try {
                        injectors.forEach(injector -> injector.inject(task.getTask()));

                        task.getMaybeRunnable().ifPresent(Runnable::run);
                        Optional<Object> result = task.getMaybeCallable().map(callable -> {
                            try {
                                return callable.call();
                            } catch (Exception exception) {
                                throw new ChromieException(String.format("An exception occurred while running task %s", task.getId()), exception);
                            }
                        });

                        // already calculate the next time in case the listeners take a long time.
                        LocalDateTime nextTime = (task.isRepeatedAtFixedRate() ? task.getTime() : LocalDateTime.now(clock)).plus(task.getRepeatAfter());

                        listeners.forEach(listener -> listener.onComplete(task.getTask().getClass(), result.orElse(null)));

                        if (task.isOneShot()) {
                            repository.delete(task.getId());
                        } else {
                            repository.save(task.reschedule(nextTime));
                        }
                    } catch (Exception exception) {
                        log.warn("An exception occurred while running task {}", task.getId(), exception);

                        listeners.forEach(listener -> listener.onException(task.getTask().getClass(), exception));
                    }
                }, delay.toMillis(), TimeUnit.MILLISECONDS);
            });
        } catch (Exception exception) {
            log.error("An error occurred while sweeping for tasks", exception);
        }
    }

    public void shutdown() {
        context.getScheduledExecutorService().shutdown();
    }
}
