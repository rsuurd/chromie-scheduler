/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie;

import dev.chromie.concurrent.ChromieScheduledExecutorService;
import dev.chromie.configuration.ChromieConfiguration;

import java.util.concurrent.ScheduledExecutorService;

public class Chromie {
    public static ScheduledExecutorService createScheduler() {
        return createScheduler(ChromieConfiguration.defaultConfiguration());
    }

    public static ScheduledExecutorService createScheduler(ChromieConfiguration configuration) {
        ChromieContext context = new ChromieContext(configuration.getClock(), configuration.getScheduledExecutorService(), configuration.getRepository());
        ChromieDaemon daemon = new ChromieDaemon(context, configuration.getSweepInterval(), configuration.getResourceInjectors(), configuration.getTaskListeners());
        daemon.start();

        Runtime.getRuntime().addShutdownHook(new Thread(() ->
            context.getScheduledExecutorService().shutdown())
        );

        return new ChromieScheduledExecutorService(context);
    }
}
