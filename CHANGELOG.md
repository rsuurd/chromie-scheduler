# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]
The Daemon now tries to claim a task before scheduling it.

## [1.0.0] - 2019-05-24
Initial release of Chromie Scheduler! 🥳🥳

[Unreleased]: https://gitlab.com/rsuurd/chromie-scheduler/compare/v1.0.0...master
[1.0.0]: https://gitlab.com/rsuurd/chromie-scheduler/tags/v1.0.0
