/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.springboot.autoconfig;

import dev.chromie.*;
import dev.chromie.concurrent.ChromieScheduledExecutorService;
import dev.chromie.repositories.ChromieRepository;
import dev.chromie.repositories.file.FileRepository;
import dev.chromie.serialization.Serializer;
import dev.chromie.springboot.ChromieProperties;
import dev.chromie.springboot.FileRepositoryProperties;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

@Configuration
@RequiredArgsConstructor
@AutoConfigureAfter(SerializerAutoConfiguration.class)
@EnableConfigurationProperties({ChromieProperties.class, FileRepositoryProperties.class})
public class ChromieAutoConfiguration {
    @NonNull
    private ChromieProperties properties;

    @Bean
    @ConditionalOnMissingBean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }

    @Bean
    @ConditionalOnMissingBean
    public ScheduledExecutorService scheduledExecutorService() {
        return Executors.newScheduledThreadPool(properties.getCorePoolSize());
    }

    @Bean
    @ConditionalOnMissingBean
    public ChromieRepository chromieRepository(FileRepositoryProperties fileRepositoryProperties, Serializer serializer) {
        return new FileRepository(fileRepositoryProperties.getPath(), serializer);
    }

    @Bean
    @ConditionalOnMissingBean
    public ChromieContext chromieContext(Clock clock, ChromieRepository repository) {
        return new ChromieContext(clock, scheduledExecutorService(), repository);
    }

    @Bean
    @ConditionalOnMissingBean
    public ChromieScheduledExecutorService chromieScheduledExecutorService(ChromieContext context) {
        return new ChromieScheduledExecutorService(context);
    }

    @Bean
    @ConditionalOnMissingBean
    public ResourceInjector springResourceInjector() {
        return new SpringResourceInjector();
    }

    @Bean(initMethod = "start")
    @ConditionalOnMissingBean
    public ChromieDaemon chromieDaemon(ChromieContext context, List<ResourceInjector> injectors, List<TaskListener> taskListeners) {
        return new ChromieDaemon(context, properties.getSweepInterval(), injectors, taskListeners);
    }
}
