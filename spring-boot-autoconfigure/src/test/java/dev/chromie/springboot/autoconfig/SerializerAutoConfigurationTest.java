/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.springboot.autoconfig;

import dev.chromie.serialization.JavaSerializer;
import dev.chromie.serialization.json.JacksonSerializer;
import dev.chromie.serialization.xml.XStreamSerializer;
import dev.chromie.springboot.ChromieProperties;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

class SerializerAutoConfigurationTest {
    private ApplicationContextRunner runner;

    @BeforeEach
    void createContext() {
        runner = new ApplicationContextRunner().withConfiguration(AutoConfigurations.of(SerializerAutoConfiguration.class));
    }

    @Test
    void shouldConfigureDefaultSerializer() {
        runner.run(context -> {
            Assert.assertEquals(ChromieProperties.SerializerType.DEFAULT, context.getBean(ChromieProperties.class).getSerializer());
            assertTrue(context.containsBean("serializer"));
            assertNotNull(context.getBean(JacksonSerializer.class));
        });
    }

    @Test
    void shouldConfigureFileSerializer() {
        runner.withPropertyValues("chromie.serializer=java").run(context -> {
            assertEquals(ChromieProperties.SerializerType.JAVA, context.getBean(ChromieProperties.class).getSerializer());
            assertTrue(context.containsBean("serializer"));
            assertNotNull(context.getBean(JavaSerializer.class));
        });
    }

    @Test
    void shouldConfigureXStreamSerializer() {
        runner.withPropertyValues("chromie.serializer=xstream").run(context -> {
            assertEquals(ChromieProperties.SerializerType.XSTREAM, context.getBean(ChromieProperties.class).getSerializer());
            assertTrue(context.containsBean("serializer"));
            assertNotNull(context.getBean(XStreamSerializer.class));
        });
    }

    @Test
    void shouldConfigureJacksonSerializer() {
        runner.withPropertyValues("chromie.serializer=jackson").run(context -> {
            assertEquals(ChromieProperties.SerializerType.JACKSON, context.getBean(ChromieProperties.class).getSerializer());
            assertTrue(context.containsBean("serializer"));
            assertNotNull(context.getBean(JacksonSerializer.class));
        });
    }
}
