/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.springboot.autoconfig;

import dev.chromie.repositories.mongo.MongoRepository;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

class MongoAutoConfigurationTest {
    private ApplicationContextRunner runner;

    @BeforeEach
    void createContext() {
        runner = new ApplicationContextRunner()
            .withUserConfiguration(MongoTestConfiguration.class)
            .withConfiguration(AutoConfigurations.of(
                SerializerAutoConfiguration.class,
                MongoAutoConfiguration.class
                )
            );
    }

    @Test
    void shouldConfigureMongoRepository() {
        runner.withPropertyValues("chromie.repository.mongo.databaseName=chromie").run(context ->
            assertNotNull(context.getBean(MongoRepository.class))
        );
    }

    @Configuration
    static class MongoTestConfiguration {
        @Bean
        public MongoClient mongoClient() {
            MongoClient mongoClient = mock(MongoClient.class);
            MongoDatabase db = mock(MongoDatabase.class);
            @SuppressWarnings("unchecked")
            MongoCollection<Document> collection = mock(MongoCollection.class);

            when(mongoClient.getDatabase(any())).thenReturn(db);
            when(db.getCollection(any())).thenReturn(collection);

            return mongoClient;
        }
    }
}
