/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.springboot.autoconfig;

import dev.chromie.repositories.jpa.JpaRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.mock;

class JpaAutoConfigurationTest {
    private ApplicationContextRunner runner;

    @BeforeEach
    void createContext() {
        runner = new ApplicationContextRunner()
            .withUserConfiguration(JpaTestConfiguration.class)
            .withConfiguration(AutoConfigurations.of(
                SerializerAutoConfiguration.class,
                JpaAutoConfiguration.class
                )
            );
    }

    @Test
    void shouldConfigureJpaRepository() {
        runner.run(context -> assertNotNull(context.getBean(JpaRepository.class)));
    }

    @Configuration
    static class JpaTestConfiguration {
        @Bean
        public EntityManagerFactory entityManagerFactory() {
            return mock(EntityManagerFactory.class);
        }

        @Bean
        public JpaAutoConfiguration.EntityManagerSupplier entityManagerSupplier() {
            return () -> mock(EntityManager.class);
        }
    }
}
