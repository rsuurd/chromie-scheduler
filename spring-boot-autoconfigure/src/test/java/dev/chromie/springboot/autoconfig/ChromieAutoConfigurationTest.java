/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.springboot.autoconfig;

import dev.chromie.TaskListener;
import dev.chromie.repositories.file.FileRepository;
import dev.chromie.springboot.ChromieProperties;
import dev.chromie.springboot.FileRepositoryProperties;
import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.autoconfigure.AutoConfigurations;
import org.springframework.boot.test.context.runner.ApplicationContextRunner;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.concurrent.ScheduledExecutorService;

import static org.assertj.core.api.Assertions.assertThat;

class ChromieAutoConfigurationTest {
    private ApplicationContextRunner runner;

    @BeforeEach
    void createContext() {
        runner = new ApplicationContextRunner().withConfiguration(AutoConfigurations.of(
            SerializerAutoConfiguration.class,
            ChromieAutoConfiguration.class)
        );
    }

    @Test
    void shouldConfigureScheduler() {
        runner.run(context ->
            assertThat(context.getBean("chromieScheduledExecutorService", ScheduledExecutorService.class)).isNotNull()
        );
    }

    @Test
    void shouldConfigureFileRepository() {
        runner.withPropertyValues("chromie.repository.file.path=/tmp").run(context -> {
            FileRepositoryProperties properties = context.getBean(FileRepositoryProperties.class);
            assertThat(properties).isNotNull();
            assertThat(properties.getPath()).isEqualTo(Paths.get("/tmp"));
            assertThat(context.getBean(FileRepository.class)).isNotNull();
        });
    }

    @Test
    void sweepIntervalCanBeConfigured() {
        runner.withPropertyValues("chromie.sweepInterval=PT0.2S").run(context -> {
            ChromieProperties properties = context.getBean(ChromieProperties.class);
            assertThat(properties).isNotNull();
            assertThat(properties.getSweepInterval()).isEqualTo(Duration.ofMillis(200));
        });
    }

    @Test
    void corePoolSizeCanBeConfigured() {
        runner.withPropertyValues("chromie.corePoolSize=5").run(context -> {
            ChromieProperties properties = context.getBean(ChromieProperties.class);
            assertThat(properties).isNotNull();
            assertThat(properties.getCorePoolSize()).isEqualTo(5);
        });
    }

    @Test
    void listeners() {
        runner.run(context -> {
            assertThat(context.getBeansOfType(TaskListener.class)).isEmpty();
        });
    }
}
