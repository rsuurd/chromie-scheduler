/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.serialization.xml;

import com.thoughtworks.xstream.XStream;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class XStreamSerializerTest {
    @Mock
    private XStream xstream;

    @InjectMocks
    private XStreamSerializer serializer;

    @Test
    void shouldSerialize() {
        Object task = new Object();

        serializer.serialize(task);

        verify(xstream).toXML(eq(task), any(ByteArrayOutputStream.class));
    }

    @Test
    void shouldDeserialize() {
        ArgumentCaptor<ByteArrayInputStream> captor = ArgumentCaptor.forClass(ByteArrayInputStream.class);

        byte[] bytes = "<xml/>".getBytes();

        serializer.deserialize(bytes);

        verify(xstream).fromXML(captor.capture());
    }
}
