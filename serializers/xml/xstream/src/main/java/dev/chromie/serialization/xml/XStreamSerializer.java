/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.serialization.xml;

import dev.chromie.serialization.Serializer;
import com.thoughtworks.xstream.XStream;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;

@RequiredArgsConstructor
public class XStreamSerializer implements Serializer {
    @NonNull
    private XStream xstream;

    public XStreamSerializer() {
        this(new XStream());
    }

    @Override
    public byte[] serialize(Object object) {
        xstream.toXML(object, new ByteArrayOutputStream());

        return new ByteArrayOutputStream().toByteArray();
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T deserialize(byte[] source) {
        return (T) xstream.fromXML(new ByteArrayInputStream(source));
    }
}
