/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.serialization.json;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.concurrent.Callable;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

class JacksonSerializerTest {
    private JacksonSerializer serializer;

    @BeforeEach
    void createSerializer() {
        serializer = new JacksonSerializer();
    }

    @Test
    void shouldSerialize() {
        byte[] bytes = serializer.serialize(new Task("json"));

        assertArrayEquals("{\"taskType\":\"dev.chromie.serialization.json.JacksonSerializerTest$Task\",\"payload\":{\"value\":\"json\"}}".getBytes(), bytes);
    }

    @Test
    void shouldDeserialize() throws Exception {
        byte[] json = Files.readAllBytes(Paths.get(getClass().getResource("/serialized-task.json").toURI()));

        Callable<String> task = serializer.deserialize(json);

        assertNotNull(task);
        assertEquals("Chromie", task.call());
    }

    static class Task implements Callable<String> {
        private String value;

        private Task() {}

        private Task(String value) {
            this.value = value;
        }

        public String call() {
            return value;
        }
    }
}
