/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.serialization.json;

import dev.chromie.ChromieException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;

public class SerializedTaskDeserializer extends StdDeserializer<SerializedTask> {
    protected SerializedTaskDeserializer() {
        this(null);
    }

    protected SerializedTaskDeserializer(Class<SerializedTask> type) {
        super(type);
    }

    @Override
    public SerializedTask deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        ObjectNode node = jsonParser.readValueAsTree();

        if (node.hasNonNull("taskType") && node.hasNonNull("payload")) {
            String className = node.get("taskType").asText();

            try {
                Class<?> taskType = deserializationContext.findClass(className);
                Object task = jsonParser.getCodec().treeToValue(node.get("payload"), taskType);

                return new SerializedTask(task);
            } catch (ClassNotFoundException exception) {
                throw new ChromieException(String.format("Could not find class '%s'", className));
            }
        } else {
            throw new ChromieException("JSON does not appear to contain a task type or payload");
        }
    }
}
