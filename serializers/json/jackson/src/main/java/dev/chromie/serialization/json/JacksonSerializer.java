/*
 * Copyright 2018-2019 The Chromie authors
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package dev.chromie.serialization.json;

import dev.chromie.ChromieException;
import dev.chromie.serialization.Serializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;

import java.io.IOException;

public class JacksonSerializer implements Serializer {
    @NonNull
    private ObjectMapper objectMapper;

    public JacksonSerializer() {
        this(new ObjectMapper());
    }

    public JacksonSerializer(ObjectMapper objectMapper) {
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        objectMapper.setVisibility(PropertyAccessor.FIELD, JsonAutoDetect.Visibility.ANY);

        this.objectMapper = objectMapper;
    }

    @Override
    public byte[] serialize(Object object) {
        try {
            SerializedTask task = new SerializedTask(object);

            return objectMapper.writer().writeValueAsBytes(task);
        } catch (JsonProcessingException exception) {
            throw new ChromieException(String.format("Could not convert %s to json", object), exception);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T deserialize(byte[] source) {
        try {
            SerializedTask task = objectMapper.readerFor(SerializedTask.class).readValue(source);

            return (T) task.getPayload();
        } catch (IOException exception) {
            throw new ChromieException("Could not deserialize", exception);
        }
    }
}
